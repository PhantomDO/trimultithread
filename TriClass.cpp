#include "TriClass.h"


static std::mutex mut;

TriClass::TriClass() : nbThread(0), temps_ecoule_mono(0), temps_ecoule_mono_q(0), temps_ecoule_multi(0), temps_ecoule_multi_q(0)
{
	std::cout << "entrez un entier positif" << std::endl;
	std::cin >> NB;
	tab.resize(NB);
	tab2.resize(NB);
	tabTrier.resize(NB);

	std::default_random_engine rd;  //Will be used to obtain a seed for the random number engine
	std::uniform_int_distribution<int> dis(0, 999); // genere un nombre r�el entre 0 et 999
	for (__int64 i = 0; i < NB; i++)
		tab2[i] = dis(rd); // on remplit le tableau de nombres aleatoires
	std::cout << "Init done." << std::endl;
}

void TriClass::Init()
{
	tab.clear();
	tab = tab2;
	LV.clear();
	//std::cout << "COPY DONE" << std::endl;
}



void TriClass::SortAlgo(size_t BEG, size_t MAX, const bool multiT)
{
	std::sort(tab.begin() + BEG, tab.begin() + MAX);
	if (multiT)
		LV.push_back(std::vector<int>(tab.begin() + BEG, tab.begin() + MAX)); // On envoie le tableau trier dans la liste (multithread uniquement)
}

void TriClass::SortAlgoMono()
{
	Init();

	debut = std::chrono::system_clock::now();

	SortAlgo(0, NB, false);

	fin = std::chrono::system_clock::now();

	tabTrier.clear();
	tabTrier = tab;

	temps_ecoule_mono = std::chrono::duration_cast<std::chrono::milliseconds>(fin - debut).count();
	std::cout << "temps de calcul sort mono: " << temps_ecoule_mono;
}

void TriClass::SortAlgoMulti()
{
	Init();

	std::vector<std::thread> threads; // tableau des threads, on lui passe des fonctions a executer en parall�les
	size_t threadPart = (NB) / nbThread; // d�coupage du tableau gr�ce � sa taille et le nb de thread utilis�s.
	size_t threadRest = NB % nbThread;
	std::cout << "threadPart : " << threadPart << ", threadRest : " << threadRest;

	debut = std::chrono::system_clock::now(); // init de temps

	for (size_t i = 0; i < nbThread; i++) // ici on envoie une partie du tab se faire trier, renvoie le tableau trier dans la list
	{	
		if (i == nbThread - 1 && threadRest != 0)
			threads.push_back(std::thread::thread(&TriClass::SortAlgo, this, i * threadPart, ((i + 1) * threadPart) + threadRest, true));
		else
			threads.push_back(std::thread::thread(&TriClass::SortAlgo, this, i * threadPart, (i + 1) * threadPart, true));
	}
	for (auto& j : threads)
		j.join(); // execution des op�ration contenu dans le tableau de thread ici la fonction SortAlgo

	threads.clear(); 

	auto I = LV.begin(), J = std::next(LV.begin(), 1);
	// ici m�me principe on dit au thread disponible de fusionner 2 tableau 
	{
		for (size_t i = 0; i < nbThread; i++) // envoie les tableau 0 et 1 de la list se faire fusionner
			threads.push_back(std::thread::thread(&TriClass::Merge, this, I, J));

		for (auto& e : threads)
			e.join();

		tab.clear();
		tab = LV[0]; // copie du tableau final trier
	}

	fin = std::chrono::system_clock::now(); // fin du temps

	temps_ecoule_multi = std::chrono::duration_cast<std::chrono::milliseconds>(fin - debut).count();

	std::cout << ", temps de calcul sort multi: " << temps_ecoule_multi;
}



int TriClass::partition(std::vector<int>& arr, const int l, const int h)
{
	int pivotIndex = l + (h - l) / 2; // pivot au ~ milieu du tableau.
	int pivotValue = arr[pivotIndex];
	int i = l, j = h;

	while (i <= j) // low <= high (debut <= fin du tab)
	{
		while (arr[i] < pivotValue)
			i++; // on avance l'index de la low tant que sa valeur est plus basse que celle du pivot
		while (arr[j] > pivotValue)
			j--; // on baisse l'index du high tant que sa valeur est plus �l�v� que celle du pivot

		if (i <= j)
		{
			// on swap les valeurs dans le tableau gr�ce au index et on effectue la m�me op�ration de d�calage.
			std::swap(arr[i], arr[j]);
			i++;
			j--;
		}
	}

	return i;
}

void TriClass::SortQuick(std::vector<int>& arr, const int l, const int h, const bool multiT)
{
	int BEG = l, MAX = h;
	std::vector<int> stack((h - l) + 1);

	// initialize top of stack 
	int top = -1;

	// push initial values of l and h to stack 
	stack[++top] = BEG;
	stack[++top] = MAX;

	int p;
	// Keep popping from stack while is not empty 
	while (top >= 0) 
	{
		// Pop h and l 
		MAX = stack[top--];
		BEG = stack[top--];
		// Set pivot element at its correct position 
		// in sorted array
		p = partition(arr, BEG, MAX);
		// If there are elements on left side of pivot, 
		// then push left side to stack 
		if (p - 1 > BEG) {
			stack[++top] = BEG;
			stack[++top] = p - 1;
		}
		// If there are elements on right side of pivot, 
		// then push right side to stack 
		if (p < MAX) {
			stack[++top] = p;
			stack[++top] = MAX;
		}
	}

	if (multiT)
		LV.push_back(std::vector<int>(arr.begin() + l, arr.begin() + h));

}

void TriClass::SortQuickMono()
{
	Init();

	debut = std::chrono::system_clock::now();

	SortQuick(tab, 0, NB - 1, false);

	fin = std::chrono::system_clock::now();

	temps_ecoule_mono_q = std::chrono::duration_cast<std::chrono::milliseconds>(fin - debut).count();
	std::cout << "temps de calcul quick sort mono: " << temps_ecoule_mono_q;
}

void TriClass::SortQuickMulti()
{
	Init();

	std::vector<std::thread> threads;
	size_t threadPart = (NB) / nbThread; // d�coupage du tableau gr�ce � sa taille et le nb de thread utilis�s.
	size_t threadRest = (NB) % nbThread;
	std::cout << "threadPart : " << threadPart << ", threadRest : " << threadRest;

	debut = std::chrono::system_clock::now();

	// FIRST STEP TRI //
	{
		for (size_t i = 0; i < nbThread; i++)
		{
			if (i == nbThread - 1 && threadRest != 0)
				threads.push_back(std::thread::thread(&TriClass::SortQuick, this, std::ref(tab), i * threadPart, ((i + 1) * threadPart) + threadRest, true));
			else
				threads.push_back(std::thread::thread(&TriClass::SortQuick, this, std::ref(tab), i * threadPart, (i + 1) * threadPart, true));
		}

		for (auto& j : threads)
			j.join();

		threads.clear();
	}
	

	auto I = LV.begin(), J = std::next(I, 1);
	{
		for (size_t i = 0; i < nbThread; i++)
			threads.push_back(std::thread::thread(&TriClass::Merge, this, I, J));

		for (auto& e : threads)
			e.join();

		tab.clear();
		tab = LV[0];
	}

	fin = std::chrono::system_clock::now();

	temps_ecoule_multi_q = std::chrono::duration_cast<std::chrono::milliseconds>(fin - debut).count();
	std::cout << ", temps de calcul quick sort multi : " << temps_ecoule_multi_q;
}



void TriClass::Merge(std::vector<std::vector<int>>::iterator itLV0, std::vector<std::vector<int>>::iterator itLV1)
{
	std::unique_lock<std::mutex> lockG(mut);

	if (LV.size() > 1)
	{ // Fusion de 2 tableau avec tri des valeurs croissante
		std::vector<int> v = Fusion((*itLV0), (*itLV1));
		//lockG.unlock();
		if (v.size() == 0)
			return;
		else
		{
			itLV1 = LV.erase(itLV1);
			itLV0 = LV.erase(itLV0);

			LV.push_back(v);
		}
	}
}

std::vector<int> TriClass::Fusion(std::vector<int>& T1, std::vector<int>& T2)
{
	int frt1 = 0, frt2 = 0;
	int bck1 = T1.size(), bck2 = T2.size();

	std::vector<int> tmp((bck1 - frt1) + (bck2 - frt2), 0);
	//std::cout << tmp.size();

	int i = 0, j = 0, k = 0;

	if (bck1 - frt1 > 0 && bck2 - frt2 > 0)
	{
		while (i < bck1 && j < bck2)
		{
			if (T1[i] < T2[j])
			{
				(tmp)[k] = (T1[i]);
				i++; k++;
			}
			else
			{
				(tmp)[k] = (T2[j]);
				j++; k++;
			}
		}

		for (; i < bck1; i++, k++)
			(tmp)[k] = (T1[i]);
		for (; j < bck2; j++, k++)
			(tmp)[k] = (T2[j]);
	}

	return (tmp);
}



void TriClass::Test()
{
	if (!std::is_sorted(tab.begin(), tab.end()))
		std::cout << ", pas trier";
	else std::cout << ", trier";

	bool testAccompli = true;
	if (tab.size() != tabTrier.size())
	{
		std::cout << ", != tabTrier";
		return;
	}

	for (size_t i = 0; i < tab.size(); i++)
	{
		if (tab[i] != tabTrier[i])
		{
			std::cout << ", != tabTrier";
			testAccompli = false;
			break;
		}
	}

	if (testAccompli)
		std::cout << ", == tabTrier";
}

void TriClass::stdSort()
{
	SortAlgoMulti();
	Test();
	std::cout << ", ratio : " << (temps_ecoule_mono / temps_ecoule_multi) << std::endl;
	temps_ecoule_multi = 0;
}

void TriClass::quickSort()
{
	SortQuickMulti();
	Test();
	std::cout << ", ratio : " << (temps_ecoule_mono_q / temps_ecoule_multi_q) << std::endl;
	temps_ecoule_multi = 0;
}

void TriClass::MonoThread()
{
	SortAlgoMono();
	Test();
	std::cout << std::endl;
	SortQuickMono();
	Test();
	std::cout << std::endl;
}

