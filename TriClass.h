#pragma once

#include <iostream>
#include <random>
#include <vector>

#include <chrono>
#include <ctime>

#include <memory>
#include <thread>
#include <mutex>

#include <algorithm>
#include <list>
#include <iterator>

class TriClass
{
	size_t NB;

	std::vector<int> tab, tab2, tabTrier;
	std::vector<std::vector<int>> LV;

	long double temps_ecoule_mono, temps_ecoule_multi; 
	long double temps_ecoule_mono_q, temps_ecoule_multi_q;
	std::chrono::time_point<std::chrono::system_clock>  debut, fin;

	size_t nbThread;

public:
	TriClass();

	void SetNbThread(size_t v) 
	{ 
		if (v > 1 && v <= std::thread::hardware_concurrency())
			nbThread = v;
		else
			nbThread = std::thread::hardware_concurrency();

		std::cout << std::endl << "multithread avec " << nbThread << "threads" << std::endl;
	};

	void Init();
	void Test();

	void SortAlgo(size_t BEG, size_t MAX, const bool multiT);
	void SortAlgoMono();
	void SortAlgoMulti();


	int partition(std::vector<int>& arr, const int l, const int h);
	void SortQuick(std::vector<int>& arr, const int l, const int h, const bool multiT);
	void SortQuickMono();
	void SortQuickMulti();

	void Merge(std::vector<std::vector<int>>::iterator itLV0, std::vector<std::vector<int>>::iterator itLV1);
	std::vector<int> Fusion(std::vector<int>& T1, std::vector<int>& T2);

	void stdSort();
	void quickSort();

	void MonoThread();

	void TestTri()
	{
		MonoThread();
		for (size_t t = 2; t <= std::thread::hardware_concurrency(); t += 2)
		{
			SetNbThread(t);
			stdSort();
			quickSort();
		}
	}
};

